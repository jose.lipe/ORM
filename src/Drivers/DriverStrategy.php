<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 12/11/17
 * Time: 23:19
 */
namespace Jose\ORM\Drivers;

use Jose\ORM\Model;

interface DriverStrategy
{
    public function save(Model $data);
    public function insert(Model $data);
    public function update(Model $data);
    public function select(array $data = []);
    public function delete(array $data);
    public function exec(string $query = null);
    public function first();
    public function all();
}