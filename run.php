<?php
/**
 * Created by PhpStorm.
 * User: felipe
 * Date: 12/11/17
 * Time: 22:29
 */
require  __DIR__ . '/vendor/autoload.php';

use App\Model\Users;
use Jose\ORM\Drivers\MysqlPdo;

$pdo = new PDO('mysql:host=localhost;dbname=orm','root','rada85');

$driver = new MysqlPdo($pdo);

$model = new Users;
$model->setDriver($driver);

// Inserção
/*$model->name = 'marcos';
$model->age = 24;
$model->email = 'm@m.com';
$model->save();*/

// Busca todos
var_dump($model->findAll());

// Busca Um
//var_dump($model->findFirst(2));

//sql com driver
//$driver->exec('TRUNCATE users;');

// Update
/*$model->id = 3;
$model->name = 'Marcos';
$model->save();*/

// Delete
/*
$model->id = 3;
$model->delete();*/
